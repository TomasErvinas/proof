# Proof

## YSC2209 AY2020-2021 semester 2

A repository for the student's work in the Proof module at Yale-NUS College.

All practice proofs and papers can be committed to this repository, and worked with herein. Some mini assignments also belong in here. You can find two already, at `YaleNUSTemplate.tex` and `git-and-latex.tex`.

### How to use git

You can find information on how to use git on the canvas page "Git".

### How to use LaTeX

You can find information on how to use LaTeX on the canvas pages "LaTeX instructions" and "Native LaTeX". If you have not yet set up LaTeX to run natively on your computer, now is the time to do so.

